const { user_game, user_game_biodata } = require('../models');

module.exports = {
    getAllUserGameBiodata: (req, res) => {
        user_game_biodata.findAll().then((userGameBiodata) => {
            res.status(200).json(userGameBiodata);
        });
    },
    getAllUserGame: (req, res) => {
        user_game.findAll().then((userGame) => {
            res.status(200).json(userGame);
        });
    },

    updateUser: (req, res) => {
    const userId = req.params.id;

    user_game
        .update(
            {
                name: req.body.name,
                email: req.body.email,
                credit: req.body.credit
        }, 
        {
        where: {
            id: userId,
        },
        })
        .then(user => { 
            user_game_biodata
                .update({
                    alamat: req.body.alamat,
                    kelamin: req.body.kelamin,
                    usia:req.body.usia
            }, 
            {
            where: {
            user_id: userId
            },
            } )
            .then(biodata => {
                res.status(200).json({ message: "berhasil mengupdate data" })
            });
        });
    },
};