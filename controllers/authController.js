const { user_game } = require('../models');

module.exports = {
  index: (req, res) => {
    res.redirect('/login');
  },
  login: (req, res) => {
    res.render('login');
  },
  loginPost: (req, res) => {
    const { username, password } = req.body;

    user_game.findAll({
        where: {
          email: username,
          password: password
        }
      }).then(userGame => {
        if (userGame.length === 0) {
          res.redirect(301, '/login')
        }
        req.session.loggedIn = true;
        res.redirect('/dashboard');
      })
  },
};
