const {user_game, user_game_biodata, user_game_history} = require ('../models');

module.exports = {
    dashboard: (req, res) => {
        if (req.session.loggedIn) {
            user_game.findAll().then(userGame => {
                res.render('dashboard', { userGame });
            })
        } else {
            res.redirect('/login');
        }
    },
    userAddView: (req, res) => {
        res.render('userAddView');
    },
    addUser: (req, res) => {
        user_game
        .create({
            name: req.body.name,
            email: req.body.email,
            credit: req.body.credit
        })
        .then(userGame => {
            user_game_biodata
                .create({
                    user_id: userGame.id,
                    alamat: req.body.alamat,
                    kelamin: req.body.kelamin,
                    usia:req.body.usia
                })
                .then(biodata => res.redirect(301, '/dashboard'));
        });
    },
    deleteUserGameBiodata: (req, res) => {
        const userId = req.params.id;

        user_game_biodata
            .destroy({
                where: {
                    user_id: userId
                },
            })
            .then(biodata => {
                user_game
                .destroy({
                    where: {
                        id: userId,
                    },
                })
                .then(user => {
                    res.redirect(301, '/dashboard');
                });
            });
    },
    updateUserGame: (req, res) => {
        const userId = req.params.id
        user_game
            .findOne({
                where: {
                    id: userId
                }
            })
            .then(user => {
                user_game_biodata.findOne({
                    where: {
                        user_id: user.id
                    }
            })
            .then(biodata => {
                res.render('update', { user, biodata });
            });
          });
    },
    updateUserGamePost: (req, res) => {
        const userId = req.paramas.id;

        user_game
            .update({
                name: req.body.name,
                email: req.body.email,
                credit: req.body.credit
            }, 
            {
                where: {
                    id: userId
                }
            })
            .then(user => {
                user_game_biodata
                    .update({
                        alamat: req.body.alamat,
                        kelamin: req.body.kelamin,
                        usia:req.body.usia
                    }, 
                    {
                        where: {
                            user_id: userId
                        },
                    })
                    .then(biodata => {
                        res.redirect(301, '/dashboard');
                    });
            });
    },
    getUserGameHistory: (req, res) => {
        if (req.session.loggedIn) {
            user_game_history.findAll().then(userGame => {
                res.render('history', { userGame });
            });
        };
    },
    historyUserGame: (req, res) => {
        const userId = req.params.id;
            
        user_game
            .findOne({
                where: {
                    id: userId
                }
            })
            .then(user => {
                user_game_history
                    .findOne({
                        where: {
                            user_id: user.id
                        }
                    })
                    .then(history => {res.render('history', { user, history });
                    });
            });
    },
};
