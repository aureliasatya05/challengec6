const express = require('express');
const app = express();
const session = require('express-session');
const {index, login, loginPost} = require('./controllers/authController');
const {dashboard, userAddView, addUser, deleteUserGameBiodata, updateUserGame, updateUserGamePost, getUserGameHistory, historyUserGame} = require ('./controllers/userController');

// Import Routers
const userApiRoute = require('./routes/userApiRoute');

app.use(express.json());

app.use(session({
  secret: 'dian',
  name: 'uniqueSessionID',
  saveUninitialized: false
}))

app.use(express.urlencoded({
  extended: false
}));

app.set('view engine', 'ejs');

// it will go to index function when user get request to http://localhost:5000/
app.get('/', index);

// Auth
app.get('/login', login);
app.post('/login', loginPost);

// User
app.get('/dashboard', dashboard);
app.get('/user/add', userAddView);
app.post('/user/save', addUser);
app.get('/user/delete/:id', deleteUserGameBiodata);
app.get('/user/update/:id', updateUserGame);
app.post('/user/update/:id', updateUserGamePost);
app.get('/user/history/:id', getUserGameHistory);
app.get('/user/history/:id', historyUserGame);

// API Routes
app.use('/api/user', userApiRoute);

app.listen(5000, () => console.log('apps berjalan di port 5000'));