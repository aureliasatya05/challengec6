'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user_game_biodata', [{
      user_id: 1,
      alamat: 'Semarang',
      kelamin: 'Perempuan',
      usia: '18',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_game_biodata', null, {});
  }
};
