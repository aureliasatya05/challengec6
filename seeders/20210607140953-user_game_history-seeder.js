'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user_game_histories', [{
      rank: 1,
      skor: '100',
      avatar: 'Ninja Turtle',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_game_histories', null, {});
  }
};