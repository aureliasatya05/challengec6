'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user_games', [{
      name: 'Dian',
      email: 'aureliasatya@gmail.com',
      password: '123456',
      credit: 100000,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('user_games', null, {});
  }
};
