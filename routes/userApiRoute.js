const express = require('express');
const router = express.Router(); // Import router

// Import controller
const {
  getAllUserGame,
  updateUser,
  getAllUserGameBiodata,
} = require('../controllers/userApiController');

// The first way to route
// router.get('/', getAllUserGame);

// If there are same endpoints and different method
router.route('/').get(getAllUserGame).post(getAllUserGameBiodata);

router.post('/update/:id', updateUser);

module.exports = router;
